
var express = require('express')

var serveStatic = require('serve-static')

var path = require('path')

var fs = require('fs')

var MarkdownIt = require('markdown-it')

var swig = require('swig')

var rd = require('rd')

swig.setDefaults({cache: false})

var md = new MarkdownIt({
	html: true,
	lanyPrefix: 'code-'
})

function stripExtname(name){
	var arr = name.split('.')
	if (arr.length) {
		if (arr.length === 2) {
			return arr[0]
		}  else if (arr.length > 2) {
			arr.pop()
			return arr.join('.')
		}
	} else {
		return ''
	}
}

function markdownToHTML(content) {
	return md.render(content || '');
}

function parseSourceContent(data){
	var split = '---\n';
	var i = data.indexOf(split);
	var info = {};
	if (i !== -1) {
		var j = data.indexOf(split, i + split.length);
		if (j !== -1) {
			var str = data.slice(i + split.length, j).trim()
			data = data.slice(j + split.length)
			str.split('\n').forEach(function(line){
				var i = line.indexOf(':');
				if (i !== -1) {
					var name = line.slice(0, i).trim();
					var value = line.slice(i + 1).trim();
					info[name] = value
				}
			})
		}
	}
	info.source = data;
	return info;
}


function renderFile(file, data){
	var options = {
		filename: file,
		autoescape: false,
		locals: data
	}
	return swig.render(fs.readFileSync(file).toString(), options)
}


function eachSourceFile(sourceDir, callback){
	rd.eachFileFilterSync(sourceDir, /\.md$/, callback)
}

function renderPost(dir, file){
	var content = fs.readFileSync(file).toString();
	var post = parseSourceContent(content.toString());
	post.content = markdownToHTML(post.source);
	post.layout = post.layout || 'post';
	var html = renderFile(path.resolve(dir, '_layout', post.layout + '.html'), {post: post})
	return html;
}


function renderIndex(dir) {
	var list = [];
	var sourceDir = path.resolve(dir, '_post');
	rd.eachFileFilterSync(sourceDir, /\.md$/, function(f, s){
		var source = fs.readFileSync(f).toString();
		var post = parseSourceContent(source);
		post.timestamp = new Date(post.date)
		post.url = '/post/' + stripExtname(f.slice(sourceDir.length + 1) + '.html')
		list.push(post)
	})
	list.sort(function(a, b){
		return b.timeStamp - a.timestamp;
	})
	var html = renderFile(path.resolve(dir, '_layout', 'index.html'), {posts: list});
	return html
}


exports.renderPost = renderPost;
exports.renderIndex = renderIndex;
exports.stripExtname = stripExtname;
exports.eachSourceFile = eachSourceFile;



