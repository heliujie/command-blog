
// var express = require('express')

// var serveStatic = require('serve-static')

// var path = require('path')

// var fs = require('fs')

// var MarkdownIt = require('markdown-it')

// var swig = require('swig')

// var rd = require('rd')

// swig.setDefaults({cache: false})

// var md = new MarkdownIt({
// 	html: true,
// 	lanyPrefix: 'code-'
// })

// module.exports = function(dir){
// 	dir = dir || '.';

// 	var app = express();
// 	var router = express.Router();
// 	app.use('./assets', serveStatic(path.resolve(dir, 'assets')));
// 	app.use(router);

// 	router.get('/post/*', function(req, res, next){
// 		var name = stripExtname(req.params[0])
// 		console.log(req.params[0])
// 		console.log(name)
// 		var file = path.resolve(dir, '_post', name + '.md')
// 		fs.readFile(file, function(err, content){
// 			if (err) return next(err);
// 			var post = parseSourceContent(content.toString())
// 			post.content = markdownToHTML(post.source)
// 			post.layout = post.layout || 'post';
// 			var html = renderFile(path.resolve(dir, '_layout', post.layout + '.html'), {post: post})
// 			res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'})
// 			res.end(html); 
// 		})
// 	})

// 	router.get('/', function(req, res, next){
// 		var list = [];
// 		var sourceDir = path.resolve(dir, '_post');
// 		rd.eachFileFilterSync(sourceDir, /\.md$/, function(f, s){
// 			var source = fs.readFileSync(f).toString();
// 			var post = parseSourceContent(source);
// 			post.timestamp = new Date(post.date)
// 			post.url = '/post/' + stripExtname(f.slice(sourceDir.length + 1) + '.html')
// 			list.push(post)
// 		})
// 		list.sort(function(a, b){
// 			return b.timeStamp - a.timestamp;
// 		})
// 		var html = renderFile(path.resolve(dir, '_layout', 'index.html'), {posts: list});
// 		res.end(html);
// 	})
	
// 	app.listen(3000);

// 	console.log('Server Listening at http://127.0.0.1:3000')
// }


// function stripExtname(name){
// 	var arr = name.split('.')
// 	if (arr.length) {
// 		if (arr.length === 2) {
// 			return arr[0]
// 		}  else if (arr.length > 2) {
// 			arr.pop()
// 			return arr.join('.')
// 		}
// 	} else {
// 		return ''
// 	}
// }

// function markdownToHTML(content) {
// 	return md.render(content || '');
// }

// function parseSourceContent(data){
// 	var split = '---\n';
// 	var i = data.indexOf(split);
// 	var info = {};
// 	if (i !== -1) {
// 		var j = data.indexOf(split, i + split.length);
// 		if (j !== -1) {
// 			var str = data.slice(i + split.length, j).trim()
// 			data = data.slice(j + split.length)
// 			str.split('\n').forEach(function(line){
// 				var i = line.indexOf(':');
// 				if (i !== -1) {
// 					var name = line.slice(0, i).trim();
// 					var value = line.slice(i + 1).trim();
// 					info[name] = value
// 				}
// 			})
// 		}
// 	}
// 	info.source = data;
// 	return info;
// }

// function renderFile(file, data){
// 	var options = {
// 		filename: file,
// 		autoescape: false,
// 		locals: data
// 	}
// 	return swig.render(fs.readFileSync(file).toString(), options)
// }





var express = require('express');
var serveStatic = require('serve-static');
var path = require('path');
var utils = require('./utils');


module.exports = function(dir){
	dir = dir || '.';
	var app = express()
	var router = express.Router()
	app.use('/assets', serveStatic(path.resolve(dir, 'assets')));
	app.use(router);

	router.get('/post/*', function(req,res,next){
		var name = utils.stripExtname(req.params[0])
		var file = path.resolve(dir, '_post', name + '.md');
		var html = utils.renderPost(dir, file);
		res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'})
		res.end(html);
	})
	router.get('/', function(req, res, next){
		var html = utils.renderIndex(dir);
		res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'})
		res.end(html)
	})
	app.listen(3000);
}





